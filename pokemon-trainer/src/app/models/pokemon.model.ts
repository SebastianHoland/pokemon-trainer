export interface Pokemon {
    name: string;
    url: string;
    caught: boolean;
}

export interface PokemonResponse {
    count: number;
    next: string;
    previous: string;
    results: Pokemon[];
}

export interface User {
    id: number;
    username: string;
    pokemon: [];
}

