import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginPage } from './pages/login/login.page';

import { PokemonsPage } from './pages/pokemons/pokemons.page';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPage,
    pathMatch: 'full'
    
  },
  
  {
    path: 'Pokèdex',
    component: PokemonsPage,
    canActivate: [ AuthGuard ]
  },

  {
  path: 'Trainer',
  component: TrainerProfilePage,
  canActivate: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
