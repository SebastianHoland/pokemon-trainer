import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, User } from '../models/pokemon.model';

const USER_KEY = "pokemon-username";
const SESSION_KEY  = "pokemon";
const URL = environment.pokeAPI;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _username: string = "";
  private _user: any;
  pokemonname: string = "";

  get username(): string{
    return this._username;
  }

  get user(): User {
    return this._user;
  }


  set username(username: string){

  }
 
  setUser(user: User) {
    this._user = user;
  }

  register(username: string): Observable<User> {
    const headers = this.createHeaders();
    localStorage.setItem(USER_KEY, username);
    this._username = username;
    return this.http.post<User>(URL, {
      username, pokemon:[]
    },{headers}
    )
  }

  logOut(){
    this._username = "";
    localStorage.removeItem(USER_KEY);
    sessionStorage.removeItem(SESSION_KEY);
  }

  findByUsername(username: string): Observable<User | undefined> {
    // return the Observable
    localStorage.setItem(USER_KEY, username);
    this._username = username;
    return this.http.get<User[]>(`${URL}/?username=${username}`)
      .pipe(
        map( (users: any) => users[0]), // Because the API returns an array of users
        tap( (user: User) => {
          this._user = user;
         /*  sessionStorage.setItem(SESSION_KEY, JSON.stringify(user.pokemon));   */        
        }
          )           
      );
  }

  addToAPI(pokemon: Pokemon): Observable<any>{
    const headers = this.createHeaders();
    const body =  {
      pokemon: [...this._user.pokemon, pokemon.name],
    };
    console.log("addtoAPi", this._user);
    
    const userUrl = ("https://lhm-noroff-api.herokuapp.com/trainers/" + this._user.id);  
    return this.http.patch<User>(userUrl, body, {headers})
    .pipe(
      tap((updatedUser: User) => {
        this._user = updatedUser;
      })
    )
  }

  deleteFromAPI (pokemon: String): Observable<any> {
    const headers = this.createHeaders();
    
    const index = this._user.pokemon.indexOf(pokemon, 0);
    if (index > -1) {
      this._user.pokemon.splice(index, 1);
    }
    const body =  {
      pokemon: this._user.pokemon,
    };
    const userUrl = ("https://lhm-noroff-api.herokuapp.com/trainers/" + this._user.id);  
    return this.http.patch<User>(userUrl, body, {headers})
    .pipe(
      tap((updatedUser: User) => {
        this._user = updatedUser;
      })
    ) 
  }

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': "eLNYKUCd6E+bXomLXUFw0Q=="
    })
  }

  constructor(
    private http: HttpClient) {  
      this._username = localStorage.getItem(USER_KEY) || "";
  }
}
