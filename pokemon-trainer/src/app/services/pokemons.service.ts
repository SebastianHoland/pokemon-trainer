import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse, User } from '../models/pokemon.model';
import { UserService } from './user.service';


const URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  private _pokemons: Pokemon[] = [];
  private _loading: boolean = false;
  private _pokemon: string[] = [];
  li: any;

  get pokemon(): string[] {
    return this._pokemon;
  }

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get loading(): boolean {
    return this._loading;
  }

  pokemonArray(){
    return this._pokemons;
  }

  public setAsCaught(pokemonName: string) {
    this._pokemons.forEach(function(obj) {
      if (obj.name === pokemonName) {
          obj.caught = true;
      }
    });
  }


  findTrainerPokemons(): void {
    /* return this.sessionStorage(this.pokemon) 
    this._pokemon = JSON.parse(sessionStorage.getItem('pokemon')); */
    console.log( 
      "inside findTrainerPokemons"
      ,sessionStorage.getItem('pokemon')
    );
    this.http.get("https://lhm-noroff-api.herokuapp.com/trainers?username=" + `${this.userService.username}`)    
    .pipe(
      map( ( response : any) => {
          return response[0].pokemon
      }))
      .subscribe( {
        next: (pokemon: string[]) => {
          this._pokemon = pokemon;
        }
      })   
  }
  
  findAllPokemons(): void {
    this._loading = true;
    this.http.get<PokemonResponse>(URL)
    .pipe(
      map(( response: PokemonResponse) => {
        return response.results
      }),
      finalize(() => {
        this._loading = false; 
      }))
      .subscribe({
        next: (pokemons: Pokemon[]) => {
          this._pokemons = pokemons;
        }
    })
  }

  

  //Dependency injection
  constructor(
    private userService: UserService,
    private http: HttpClient) { }
}


