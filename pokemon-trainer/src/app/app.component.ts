import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokemon-trainer';

  get username(): string {
    return this.userService.username;
  }

  logout(){
    this.userService.logOut();
    this.router.navigateByUrl("/");
  }

  pokedex(){ this.router.navigateByUrl("/Pokèdex");}

  trainer(){ this.router.navigateByUrl("/Trainer");}
  
  //Dependency injection
  constructor(
    private userService: UserService,
    private router: Router){}

}

