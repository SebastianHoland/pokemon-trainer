import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonsService } from 'src/app/services/pokemons.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  makeImgUrl(pokemons: { url: string; }): any {
    const pokemonNumber = pokemons.url.split('/')
    return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemonNumber[6] + ".png"
  }

  @Input() pokemons: Pokemon[] = []; 
  @Output() caught: EventEmitter<Pokemon> = new EventEmitter();

  onPokemonClick(pokemon: Pokemon) {
    console.log("POKEMON CLICKED", pokemon.name);
    this.caught.emit(pokemon); //Notifying the parent
    this.pokemonService.setAsCaught(pokemon.name);
  }


  constructor(private readonly pokemonService: PokemonsService) {  }

  ngOnInit(): void {

  }

}
