import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PokemonsService } from 'src/app/services/pokemons.service';


@Component({
  selector: 'app-trainer-list',
  templateUrl: './trainer-list.component.html',
  styleUrls: ['./trainer-list.component.css']
})
export class TrainerListComponent implements OnInit {

  @Input() pokemon: string[] = [] ;
  @Output() freeThePokemon: EventEmitter<any> = new EventEmitter();

  makeImgUrl(namePokemon: string): any { 
    let pokemonNumber: string[] = [];
    const pokemons = this.pokeService.pokemons;
    const pokemon = pokemons.find( pokemonen => pokemonen.name === namePokemon);
    if(pokemon !== undefined){
      pokemonNumber = pokemon.url.split('/')
    }
    return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemonNumber[6] + ".png";

  }

  constructor(
    private pokeService: PokemonsService
   
  ) { }

  onPokemonClickDelete(pokemon: string) {
    console.log(pokemon, " is free!");
    this.freeThePokemon.emit(pokemon); //Notifying the parent
  }
  
  ngOnInit(): void {
    }
  
}