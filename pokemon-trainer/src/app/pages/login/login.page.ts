import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void { 

  }

  onLoginSubmit(form: NgForm): void {
    const { username } = form.value;


    this.userService.findByUsername(username)
    .subscribe({
      next: (user: User | undefined) => {
        if(user === undefined){
          this.userService.register(username)
            .subscribe({
              next: (response: any) => {
              console.log("Register", response);
              this.userService.setUser(response);

              }
            });
            this.userService.username = username;
            this.router.navigateByUrl("/Pokèdex");
        }else{
          this.userService.setUser(user);
          this.userService.username = user.username;
          this.router.navigateByUrl("/Pokèdex");
        }
      }
    })
  }
}
