import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/pokemon.model';
import { PokemonsService } from 'src/app/services/pokemons.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.page.html',
  styleUrls: ['./trainer-profile.page.css']
})
export class TrainerProfilePage implements OnInit {
  
  get pokemon(): string[] {
    /* return JSON.parse(sessionStorage.getItem('pokemon'));  */
    return this.pokemonService.pokemon;
  } 

  get username(): string {
    return this.userService.username;
  }
  
  constructor(
    private userService: UserService,
    private pokemonService: PokemonsService) { }
  
  ngOnInit(): void {
    this.pokemonService.findTrainerPokemons(); 
    this.pokemonService.findAllPokemons();
  }

  onFree (pokemon: string){
    alert(pokemon + " is free");
    this.userService.deleteFromAPI(pokemon)
    .subscribe((response: User) => {
    });
    
  }
}
