import { Component, OnInit } from '@angular/core';
import { Pokemon, User } from 'src/app/models/pokemon.model';
import { PokemonsService } from 'src/app/services/pokemons.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.page.html',
  styleUrls: ['./pokemons.page.css']
})
export class PokemonsPage implements OnInit {

  catch: boolean = false;

  get pokemons(): Pokemon[] {
    return this.pokemonService.pokemons;
  }

  get username(): string {
    return this.userService.username;
  }

  //Dependency injection
  constructor(
    private userService: UserService,
    private pokemonService: PokemonsService) { }

  ngOnInit(): void {
    this.pokemonService.findAllPokemons();
  }

  onCaught(pokemon: Pokemon)  {
    
    this.userService.addToAPI(pokemon)
      .subscribe((response: User) => {
        alert("You have caught " + pokemon.name +  "!")
      })
  }
}
