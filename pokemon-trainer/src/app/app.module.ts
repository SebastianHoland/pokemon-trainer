import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';

import { PokemonsPage } from './pages/pokemons/pokemons.page';
import { LoginPage } from './pages/login/login.page';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';
import { TrainerListComponent } from './components/trainer-list/trainer-list.component';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonsPage,
    LoginPage,
    TrainerProfilePage,
    TrainerListComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
