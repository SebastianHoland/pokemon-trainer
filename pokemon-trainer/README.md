# PokemonTrainer

## Authors
Line Haadem Mattsson & Paul Sebastian Hornnes Holand

### Features
-> Login
    In the login page you can login/register (with) a new user, which is updated in the API with the name, ID and an empty pokemon array. If the user already exists it will check the api for the existing user, fetch the data from the api and forward you directly to the Pokédex page. The navbar is not visible here because a user that is not logged in, should not have the opportunity to access these pages.

-> Pokédex
    In the Pokédex page, all the original 151 pokémon is visible inside their own boxes. Inside each of these boxes the pokémons sprites is visible. Also a button to catch the pokemon is visible here. When you click the "catch button" it will be stored to the api.

-> Trainer page
    In this page, all the pokemon that you have caught will be visible in their own box and a button to set them free. A problem with freeing the pokemon is that when you click to free a pokemon, you must navigate, using the navbar, to the "Pokedex" and back to the "Trainer" in order to get the pokemon deleted from this page. If you don't do this pokemon will still be visible. 

### Flaws 
 -> If you refresh the page we get an error: "ERROR TypeError: Cannot read properties of undefined (reading 'pokemon')". You then have to log out, using the navbar, and log in again to fix that. A way to fix this, will be by using and reading from the sessionStorage, where we have saved all the pokemons you have caught. But by the time we figured that out, the deadline was already here, and time too short.

 -> When you switch between the pages, the pokeball that indicates that a pokemon has been caugt in the pokedex page will disapear. This could have been fixed by using the sessionstorage for the pokemon.
 
 -> You can also modify the url to access the login page, even though you are logged in. This could maybe be solved by using an auth guard, but we could not figure out how to do this. 



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
